/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80022
 Source Host           : localhost:3306
 Source Schema         : db_inventario

 Target Server Type    : MySQL
 Target Server Version : 80022
 File Encoding         : 65001

 Date: 08/10/2021 10:29:22
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for inventario
-- ----------------------------
DROP TABLE IF EXISTS `inventario`;
CREATE TABLE `inventario`  (
  `IdInventario` int NOT NULL AUTO_INCREMENT,
  `Clave` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Descripcion` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Lote` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Caducidad` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Cantidad` int NOT NULL,
  PRIMARY KEY (`IdInventario`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of inventario
-- ----------------------------
INSERT INTO `inventario` VALUES (1, '0104A', 'Paracetamol', 'AA22333', '10/sep/2022', 25);
INSERT INTO `inventario` VALUES (2, '0104A', 'Aspirina', 'FG23435', '10/mar/2022', 52);
INSERT INTO `inventario` VALUES (3, '0104B', 'Metmorfina', 'LP12345', '10/ago/2022', 34);
INSERT INTO `inventario` VALUES (4, '0104C', 'Vyndaqel', 'AS09378', '10/ene/2022', 65);
INSERT INTO `inventario` VALUES (5, '0104D', 'Cisaprida', 'FY02748', '10/dic/2022', 298);
INSERT INTO `inventario` VALUES (7, '0104F', 'Naproxeno', 'NM54367', '10/nov/2022', 92);
INSERT INTO `inventario` VALUES (24, '0103Y', 'Rexon', 'DW19572', '29/mar/2024', 89);
INSERT INTO `inventario` VALUES (25, '0103M', 'Salox', 'RE23856', '21/abr/2023', 56);
INSERT INTO `inventario` VALUES (26, '0507Y', 'Trioxina', 'LW65719', '26/feb/2024', 34);
INSERT INTO `inventario` VALUES (27, '1007H', 'Cloxon', 'LO95823', '12/oct/2024', 19);
INSERT INTO `inventario` VALUES (29, '0104M', 'Totoalanem', 'DL04918', '22/abr/2029', 56);
INSERT INTO `inventario` VALUES (30, '0405F', 'Toxina', 'DG74891', '20/nov/2024', 34);
INSERT INTO `inventario` VALUES (31, '0405F', 'Toxina', 'DG74891', '20/nov/2024', 34);
INSERT INTO `inventario` VALUES (32, '0105D', 'Sado', 'HK28491', '10/den/2025', 18);
INSERT INTO `inventario` VALUES (33, '0105D', 'Sado', 'HK28491', '10/den/2025', 18);
INSERT INTO `inventario` VALUES (36, '0108U', 'Treomina', 'KE95481', '03/dic/2026', 89);
INSERT INTO `inventario` VALUES (37, 'S3498', 'Anapsique', 'LM95358', '04/oct/2023', 5692);
INSERT INTO `inventario` VALUES (39, '0104M', 'Paracetamol', 'DL04918', '22/abr/2029', 1900);
INSERT INTO `inventario` VALUES (40, '0507Y', 'Naproxeno', 'LW65719', '01/sep/2026', 6000);
INSERT INTO `inventario` VALUES (41, '0106H', 'Dexametasona', 'KD23456', '12/jul/2027', 45);
INSERT INTO `inventario` VALUES (48, '1711A', 'Acido folico', '6567AWA', '01/ene/2026', 1001);
INSERT INTO `inventario` VALUES (49, '0607M', 'Oteztar', 'DP38591', '10/abr/2027', 1000);
INSERT INTO `inventario` VALUES (50, '0109D', 'Yictaxon', 'SD56783', '23/oct/2025', 235);
INSERT INTO `inventario` VALUES (51, '0109D', 'Yictaxon', 'SD56783', '23/oct/2025', 235);
INSERT INTO `inventario` VALUES (52, '0106H', 'Dexametasona', 'KD23456', '12/jul/2027', 45);
INSERT INTO `inventario` VALUES (53, '0574', 'captopril', '9999', '01/sep/2026', 190);
INSERT INTO `inventario` VALUES (54, '0101A', 'Zampox', 'KE12958', '10/may/2025', 23);
INSERT INTO `inventario` VALUES (55, '0101A', 'Zampox', 'KE12958', '10/may/2025', 23);

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios`  (
  `IdUSUARIOS` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `Contraseña` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `NoTrabajador` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  PRIMARY KEY (`IdUSUARIOS`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES (1, 'Geovani', 'sas123', 'SAS123');
INSERT INTO `usuarios` VALUES (2, 'Juan', 'sas12345', 'SAS12345');

SET FOREIGN_KEY_CHECKS = 1;
